import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError
import time

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def approval_message(ch, method, properties, body):
    print("recieved")
    data = json.loads(body)
    presenter_name = data["presenter_name"]
    email = data["presenter_email"]
    title = data["title"]
    send_mail(
        "Your presentataion was accepted",
        f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


def rejection_message(ch, method, properties, body):
    print("recieved")
    data = json.loads(body)
    presenter_name = data["presenter_name"]
    email = data["presenter_email"]
    title = data["title"]
    send_mail(
        "Your presentataion was rejected",
        f"{presenter_name}, Sorry your presentation {title} has been rejected",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


def main():
    while True:
        try:
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="presentation_approvals")
            channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=approval_message,
                auto_ack=True,
            )
            channel.queue_declare(queue="presentation_rejections")
            channel.basic_consume(
                queue="presentation_rejections",
                on_message_callback=rejection_message,
                auto_ack=True,
            )
            channel.start_consuming()
        except AMQPConnectionError:
            print("Could not connect to RabbitMQ")
            time.sleep(2.0)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

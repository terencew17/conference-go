# Generated by Django 4.1.6 on 2023-02-10 01:32

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("events", "0003_remove_location_lat_remove_location_lon_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="conference",
            name="weather",
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name="conference",
            name="description",
            field=models.TextField(null=True),
        ),
    ]

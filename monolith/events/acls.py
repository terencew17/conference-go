import requests
import json
from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY


def get_weather(city, state):
    iso = "3166-2"

    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{iso}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geo_url)
    content = json.loads(response.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    location_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    response2 = requests.get(location_url)
    location = json.loads(response2.content)
    weather = {
        "description": location["weather"][0]["description"],
        "temperature": location["main"]["temp"],
    }
    try:
        return weather
    except:
        {"temperature": None},
        {"description": None}


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    # try to return the data being requested
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    # If data is corrupt return None
    except:
        {"picture_url": None}
